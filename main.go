package main

import (
	"context"
	"math/rand"
	"time"
)

// Customer defines methods that represent actual customer that needs to be served by someone. They can request to
// be served, provide workload and state how important they are (meaning how fast they need to be served).
type Customer interface {
	// Workload returns a channel of work chunks that are to be processed through TheExpensiveFragileService.
	Workload() <-chan int
	// Weight is unit-less number that determines how much processing capacity should a customer be allocated
	// when running in parallel with other customers. The higher the weight, the more capacity the customer gets.
	Weight() int
}

func main() {
	rand.Seed(time.Now().UnixNano())

	nbCustomers := rand.Intn(10)
	customers := make([]*customer, 0, nbCustomers)
	for i := 0; i < nbCustomers; i++ {
		customers = append(customers, newCustomer())
	}
	defer func() {
		for i := range customers {
			customers[i].stop()
		}
	}()

	tefs := &TheExpensiveFragileService{
		concurrency: rand.Intn(150),
	}

	b := NewBalancer(tefs)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	for i := range customers {
		b.Register(ctx, customers[i])
	}

	<-ctx.Done()
}
