package main

import (
	"math/rand"
	"time"
)

// customer implements customer interface.
type customer struct {
	needsService *time.Ticker
	workload     int
	weight       int
}

// newCustomer initializes the customer.
func newCustomer() *customer {
	return &customer{
		needsService: time.NewTicker(time.Duration(1+rand.Intn(3)) * time.Second),
		workload:     rand.Intn(100),
		weight:       rand.Intn(10),
	}
}

// stop the underlying needsService.
func (c *customer) stop() {
	c.needsService.Stop()
}

// Workload feeds work chunks through returned channel until there is no more work to be fed.
func (c *customer) Workload() <-chan int {
	workloads := make(chan int)
	go func() {
		defer close(workloads)
		for range c.needsService.C {
			workloads <- c.workload
			if c.workload -= 1; c.workload == 0 {
				return
			}
		}
	}()
	return workloads
}

// Weight of this customer, i.e. how important they are.
func (c *customer) Weight() int {
	return c.weight
}
