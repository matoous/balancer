package main

import (
	"context"
)

// Balancer should utilize the expensive service as much as it can while making sure it does not overflow it with
// work chunks. There is a hard limit for max parallel chunks that can't ever be crossed since there's a SLO defined
// and we don't want to make the expensive service people angry.
//
// There can be arbitrary number of Customers registered at any given time, each with it's own weight. Make sure to
// correctly assign processing capacity to a customer based on other customers currently in process.
//
// To give an example of this, imagine there's a maximum number of work chunks set to 100 and there are two customers
// registered, both with the same priority. When they are both served in parallel, each of them gets to send
// 50 chunks at the same time.
//
// In the same scenario, if there were two customers with priority 1 and one customer with priority 2, the first
// two would be allowed to send 25 chunks and the other one would send 50. It's likely that the one sending 50 would
// be served faster, finishing the work early, meaning that it would no longer be necessary that those first two
// customers only send 25 each but can and should use the remaining capacity and send 50 again.
type Balancer struct {

}

// NewBalancer takes a reference to the main service.
func NewBalancer(_ *TheExpensiveFragileService) *Balancer {
	// implement me
	return &Balancer{}
}

func (b *Balancer) Run() {
	// implement me
}

// Register a customer to the balancer and start processing it's work chunks through the service.
func (b *Balancer) Register(_ context.Context, _ Customer) {
	// implement me
}
